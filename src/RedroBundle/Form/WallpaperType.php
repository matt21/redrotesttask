<?php

namespace RedroBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class WallpaperType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', null, array('label' => 'Nazwa'))
            ->add('description', null, array('label' => 'Opis'))
            ->add('color', null, array('label' => 'Kolor'))
            ->add('availability', null, array('label' => 'Ilość dostępnych sztuk'))
            ->add('type', null, array('label' => 'Typ'))
            ->add('basePrice', null, array('label' => 'Cena podstawowa'))
            ->add('isActive', null, array('label' => 'Aktywna'))
            ->add('submit', 'submit', array('label' => 'Zapisz', 'attr' => array('class' => 'btn btn-primary')));
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'RedroBundle\Entity\Wallpaper'
        ));
    }
}
