<?php

namespace RedroBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('RedroBundle:Default:index.html.twig');
    }
}
