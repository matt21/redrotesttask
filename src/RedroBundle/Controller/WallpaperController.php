<?php

namespace RedroBundle\Controller;

use RedroBundle\Event\PriceEvent;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use RedroBundle\Entity\Wallpaper;
use RedroBundle\Form\WallpaperType;

/**
 * Wallpaper controller.
 *
 */
class WallpaperController extends Controller
{
    /**
     * Lists all Wallpaper entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $wallpapers = $em->getRepository('RedroBundle:Wallpaper')->findAll();

        return $this->render('wallpaper/index.html.twig', array(
            'wallpapers' => $wallpapers,
        ));
    }

    /**
     * Creates a new Wallpaper entity.
     *
     */
    public function newAction(Request $request)
    {
        $wallpaper = new Wallpaper();
        $form = $this->createForm('RedroBundle\Form\WallpaperType', $wallpaper);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($wallpaper);

            $this->get('event_dispatcher')->dispatch('wallpaper_price', new PriceEvent($wallpaper));

            $em->flush();

            return $this->redirectToRoute('wallpaper_show', array('id' => $wallpaper->getId()));
        }

        return $this->render('wallpaper/new.html.twig', array(
            'wallpaper' => $wallpaper,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Wallpaper entity.
     *
     */
    public function showAction(Wallpaper $wallpaper)
    {
        $deleteForm = $this->createDeleteForm($wallpaper);

        return $this->render('wallpaper/show.html.twig', array(
            'wallpaper' => $wallpaper,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Wallpaper entity.
     *
     */
    public function editAction(Request $request, Wallpaper $wallpaper)
    {
        $deleteForm = $this->createDeleteForm($wallpaper);
        $editForm = $this->createForm('RedroBundle\Form\WallpaperType', $wallpaper);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($wallpaper);

            $this->get('event_dispatcher')->dispatch('wallpaper_price', new PriceEvent($wallpaper));
            $this->get('event_dispatcher')->dispatch('wallpaper_updates', new PriceEvent($wallpaper));

            $em->flush();

            return $this->redirectToRoute('wallpaper_index', array('id' => $wallpaper->getId()));
        }

        return $this->render('wallpaper/edit.html.twig', array(
            'wallpaper' => $wallpaper,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a Wallpaper entity.
     *
     */
    public function deleteAction(Request $request, Wallpaper $wallpaper)
    {
        $form = $this->createDeleteForm($wallpaper);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($wallpaper);
            $em->flush();
        }

        return $this->redirectToRoute('wallpaper_index');
    }

    /**
     * Creates a form to delete a Wallpaper entity.
     *
     * @param Wallpaper $wallpaper The Wallpaper entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Wallpaper $wallpaper)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('wallpaper_delete', array('id' => $wallpaper->getId())))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Usuń', 'attr' => array('class' => 'btn btn-block btn-danger')))
            ->getForm()
        ;
    }
}
