<?php
namespace RedroBundle\Event;

use Symfony\Component\EventDispatcher\Event;
use RedroBundle\Entity\Wallpaper;

class PriceEvent extends Event
{
    protected $wallpaper;

    public function __construct(Wallpaper $wallpaper)
    {
        $this->wallpaper = $wallpaper;
    }

    public function getWallpaper()
    {
        return $this->wallpaper;
    }
}