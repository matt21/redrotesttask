<?php

namespace RedroBundle\Event\Listener;

use RedroBundle\Event\PriceEvent;
use Doctrine\ORM\EntityManager;

class ChangeWallpaperListener
{
    protected $em;

    function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function generatePrice(PriceEvent $event)
    {
        $currentWallpaper = $event->getWallpaper();

        $wallpapers = $this->em->getRepository('RedroBundle:Wallpaper')->findBy(['isActive' => 1]);


        $availability = 0;
        foreach ($wallpapers as $wallpaper) {
            $availability += $wallpaper->getAvailability();
        }

        $priceCounter = $availability / 1000;

        $oldPrice = $currentWallpaper->getBasePrice();

        $newPrice = $oldPrice * $priceCounter ;

        $currentWallpaper->setPriceByAvailability($newPrice);


    }

    public function countUpdates(PriceEvent $event)
    {
        $currentWallpaper = $event->getWallpaper();

        $currentUpdates = $currentWallpaper->getUpdates();

        if($currentUpdates === null){
            $currentUpdates = 1;
        }else{
            $currentUpdates += 1;
        }

        $currentWallpaper->setUpdates($currentUpdates);


    }
}